from flask import Flask, request

app = Flask(__name__)


@app.route('/info',  methods=["GET"])
def info():  # put application's code here
    return ({'nombre': 'Diego Alejandro Sierra García', 'carnet':201903969})


@app.route('/sisale',  methods=["GET"])
def sisale():  # put application's code here
    return ({'mensaje': 'Si sale AyD1 con 100azo'})


if __name__ == '__main__':
    app.run(port=4000)
from flask import Flask, request

app = Flask(__name__)


@app.route('/',  methods=["POST"])
def suma():  # put application's code here
    dic = request.get_json()
    num1 = dic.get('num1')
    num2 = dic.get('num2')
    resultado = num1 + num2
    return ({'resultado':resultado})


@app.route('/resta',  methods=["POST"])
def resta():  # put application's code here
    dic = request.get_json()
    num1 = dic.get('num1')
    num2 = dic.get('num2')
    resultado = num1 - num2
    return ({'resultado':resultado})


if __name__ == '__main__':
    app.run(port=3000)